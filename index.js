//load the uplynk API helper
var uplynk = require('./uplynk_api_utils.js');

//load AWS sdk and your keys
var AWS = require('aws-sdk');
AWS.config.update({
	accessKeyId: '<NEED YOUR KEY>', 
	secretAccessKey: '<NEED YOUR KEY>',
	region: 'us-east-1'
});
var sns = new AWS.SNS();
var payload = {};


/*** UPLYNK **/
OWNER_API = '<NEED YOUR KEY>'; //your API userID from CMS>settings>account settings>UserID
OWNER_KEY = '<NEED YOUR KEY>'; //your API key from CMS>settings>Playback tokens>API Key
ROOT_URL = "http://services.uplynk.com"

//uplynk request
var request = {
	owner: OWNER_API,  //required
	key : OWNER_KEY,
	endpoint: "/api2/asset/changes", //required
	params : {
		start: (new Date).getTime() // now in ms
	}
};

//run every 60 seconds
var tid = setInterval(checkForNew, 60000);

//check for new assets every 60 seconds.  If we find one log and also fire off to Amazon SNS
//probably need to also handle the update and changed properties not just new
function checkForNew(){
	uplynk.call(request, function(response){
		if (Object.getOwnPropertyNames(response.assets).length > 0) {
			console.log("-- NEW OR UPDATED ASSETS --");
			for (var asset in response.assets){
				console.log("assetID: " + asset);
				console.log("externalID: " + response.assets[asset].external_id);
				console.log("--------------");
				//send notice to Amazon SNS, Amazon Lambda, or any other service
				//in this example, we just fire off an SMS to all subscribers via Amazon SNS
				payload.default = "Updated. assetID:" + asset + " extID:" + response.assets[asset].external_id;
				payload = JSON.stringify(payload);
				var params = {
					Message: payload,
					MessageStructure: 'json',
					TargetArn: 'arn:aws:sns:us-east-1:158970761677:uplynk-demo-sns-new-asset'
				};

				sns.publish(params, function(err, data) {
					if (err) {
						console.log(err, err.stack); // an error occurred
					}else{
						console.log(data); 
					}          // successful response
				});
			}
		}else{
			console.log("no new assets since: " + response.start);
		}
		request.params.start = response.end;	
	});
}

