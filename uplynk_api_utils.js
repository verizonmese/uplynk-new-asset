//core node modules
var http = require('http');
var zlib = require('zlib');
var crypto = require('crypto');
var querystring = require('querystring');
var request = require('request');

var generateNewUpLynkMsg = function (req) {
    var msg = {
        _owner: req.owner,
        _timestamp: (new Date().getTime())/1000
    };
    if (req.params){
        for (param in req.params) {
            msg[param] = req.params[param];
        }
    }
  
    return msg;
};

var generateSignature = function (msg, key) {
    if(!msg) {
        throw "In order to generate a signature for UpLynk, a proper UpLynk message must be provided";
 }
    return crypto.createHmac('SHA256', key).update(msg).digest('hex');
};

module.exports = {
    createSig : function(req, key, callback){  
        var msg = JSON.stringify(generateNewUpLynkMsg(req)); 
        // console.log("msg:", msg);
        msg = new Buffer(msg);
        //console.log("buffer msg: ", msg);

        zlib.deflate(msg, function zlibDeflateMsg(err, buffer) {
            if (!err) {
                buffer = buffer.toString('base64'); //base64 Encode
                var signature = generateSignature(buffer, key);
                var body = {
                    msg: buffer,
                    sig: signature
                };
                callback(querystring.stringify(body));
            }
        });
    },
    post : function(req, sig, callback){
        request.post(ROOT_URL + req.endpoint + "?" + sig, function(error, res, body){
            if (!error && res.statusCode == 200){
                callback(JSON.parse(body)); //without JSON.parse it just returns it as a string
            }else{
                console.log(error);
            }
        });
    },
    //replicating the uplynk python Call method
    call : function(req, callback){
        module.exports.createSig(req, req.key, function(sig){
            module.exports.post(req,sig, callback);
        });
    }
};